package at.ertu.basics.test;
public class Test {
	public static void main(String[] args) {
		Engine e1 = new Engine(150, "diesel");
		Engine e2 = new Engine(100, "benzin");
		
		Car c1 = new Car("red", e1);
		
		c1.getEngine().setHorsePower(157);
		Car c2 = new Car("blue", e2);
		
		System.out.println(c1.getEngine().getHorsePower());
	
	}
	
	
}
