package at.ertu.basics.car_sample;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

public class Person {
	private String firstName, lastName;
	private List<Car> cars;
	private LocalDate birthday;

	public Person(String firstName, String lastName, LocalDate birthday) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.cars = new ArrayList<Car>();
		this.birthday = birthday;
	}
	
	public void printCars() {
		for (Car car : cars) {
			System.out.println(car.getColor());
		}
	}
	
	public void addCar(Car c) {
		this.cars.add(c);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}
	
	public int getAge() {
	    Period period = Period.between(birthday, LocalDate.now());
	    return period.getYears();
	    
	  }
	
	public int getValueOfCars() {
		int value = 0;
		for (Car car : cars) {
			value += car.getPrice();
		}
		return value;
	}
	
}
