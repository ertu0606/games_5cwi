package at.ertu.basics.car_sample;

public class Engine {
	private String type;
	private int performance;
	
	public Engine(String type, int performance) {
		super();
		this.type = type;
		this.performance = performance;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getPerformance() {
		return performance;
	}

	public void setPerformance(int performance) {
		this.performance = performance;
	}
	
	
}
