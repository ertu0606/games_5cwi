package at.ertu.basics.car_sample;

public class Car {
	private String color;
	private int maxSpeed;
	private double basicprice;
	private double consumption;
	private Engine engine;
	private Producer producer;
	private double distance;
	
	public Car(String color, int maxSpeed, double basicprice, double consumption, Engine engine, Producer producer, double distance) {
		super();
		this.color = color;
		this.maxSpeed = maxSpeed;
		this.basicprice = basicprice;
		this.consumption = consumption;
		this.engine = engine;
		this.producer = producer;
		this.distance = distance;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public double getbasicprice() {
		return basicprice;
	}

	public void setbasicprice(double basicprice) {
		this.basicprice = basicprice;
	}

	public double getConsumption() {
		if(distance>50000) {
			consumption = consumption*1.098;
			return consumption;
		}
		else {
			return consumption;
		}
	}

	public void setConsumption(double consumption) {
		this.consumption = consumption;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public Producer getProducer() {
		return producer;
	}

	public void setProducer(Producer producer) {
		this.producer = producer;
	}
	
	public double getPrice() {
		double price;
		price = basicprice * (1.00-producer.getDiscount());
		return price;
	}
	
	
}
