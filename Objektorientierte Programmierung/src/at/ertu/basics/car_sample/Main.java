package at.ertu.basics.car_sample;

import java.time.LocalDate;

public class Main {
	public static void main(String[]args) {
		Engine e1 = new Engine("diesel",130);
		Engine e2 = new Engine("benzin",150);
		
		Producer p1 = new Producer("mercedes","germany",15);
		Producer p2 = new Producer("citroen","france",25);
				
		
		Car c1 = new Car("red", 210,10000,6,e1,p1,65000);
		Car c2 = new Car("green",230,3000,4,e2,p2,30000);
		
		Person pn1 = new Person("Max", "Kruse",LocalDate.of(1990, 10, 01));
		Person pn2 = new Person("Vedat", "Muriqi",LocalDate.of(1999, 12, 15));
		
		pn1.addCar(c1);
		pn1.addCar(c2);
		
		pn1.printCars();
		
		System.out.println(c1.getPrice());
		System.out.println(c1.getEngine().getType());
		System.out.println(c1.getbasicprice());
		System.out.println(c1.getConsumption());
		System.out.println(pn1.getAge());
		System.out.println(pn2.getAge());
		System.out.println(pn1.getValueOfCars()); //mit Rabatt
		
		
		
	}
}
