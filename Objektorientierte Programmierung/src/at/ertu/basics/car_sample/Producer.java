package at.ertu.basics.car_sample;

public class Producer {
	private String name;
	private String country;
	private double discount;
	
	public Producer(String name, String country, double discount) {
		super();
		this.name = name;
		this.country = country;
		this.discount = discount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public double getDiscount() {
		discount = discount * 0.01;
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
	
	
}
