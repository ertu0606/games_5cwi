package at.ertu.basics.remote;

public class Remote {
	private boolean isOn;
	private boolean hasPower;
	private Battery battery;
	private Battery battery1;
	
	public Remote(boolean isOn, boolean hasPower, Battery battery, Battery battery1) {
		super();
		this.isOn = isOn;
		this.hasPower = hasPower;
		this.battery = battery;
		this.battery1 = battery1;
	}

	public boolean isOn() {
		return isOn;
	}

	public void setOn(boolean isOn) {
		this.isOn = isOn;
	}

	public boolean isHasPower() {
		return hasPower;
	}

	public void setHasPower(boolean hasPower) {
		this.hasPower = hasPower;
	}
	
	public void turnOn() {
		isOn=true;
	}
	
	public void turnOff() {
		isOn=false;
	}
	
	public int getStatus() {
		int status = (battery.getChargingStatus() + battery1.getChargingStatus()) / 2;
		return status;			
	}
	
}
