package at.ertu.ObserverPattern;

public interface Component {
	public String isAlive();
	public String start();
}
