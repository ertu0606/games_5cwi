package at.ertu.ObserverPattern;

public class Trafficlight implements Observable {

	@Override
	public void inform() {
		System.out.println("Ampel leuchtet!");
		
	}

}
