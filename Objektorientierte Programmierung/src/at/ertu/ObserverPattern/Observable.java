package at.ertu.ObserverPattern;

public interface Observable {
	public void inform();
}
