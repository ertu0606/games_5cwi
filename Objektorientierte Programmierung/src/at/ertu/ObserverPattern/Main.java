package at.ertu.ObserverPattern;

public class Main {
	public static void main(String[] args) {
		Sensor s1 = new Sensor();
		Observable l1 = new Latern();
		Observable c1 = new Christmas();
		Observable t1 = new Trafficlight();
		
		s1.addObservable(l1);
		s1.addObservable(c1);
		s1.addObservable(t1);
		s1.informAll();
	}
}
