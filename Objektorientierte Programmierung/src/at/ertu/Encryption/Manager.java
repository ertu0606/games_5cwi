package at.ertu.Encryption;


public class Manager {
	private Encrypter encrypter;
	
	public Manager() {
		
	}

	public String doEncrypt(String b) {
		
		return this.encrypter.encrypt(b);
	}
	
	public void setEncrypter(Encrypter e) {
			this.encrypter = e;
	}
}
