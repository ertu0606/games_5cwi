package at.ertu.Encryption;

public interface Encrypter {
	public String encrypt(String data);
	public String getFounder();
}
