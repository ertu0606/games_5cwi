package at.ertu.Viergewinnt;

public class Position {
	
	int col;
	int row;
	
	public Position(int col, int row) {
		super();
		this.col = col;
		this.row = row;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}
}
