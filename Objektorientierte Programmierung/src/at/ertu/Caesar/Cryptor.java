package at.ertu.Caesar;

public interface Cryptor {
	public void Decrypt(String decrypt);
	public void Encrypt(String encrypt);
}
