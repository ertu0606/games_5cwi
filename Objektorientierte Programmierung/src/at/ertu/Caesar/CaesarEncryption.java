package at.ertu.Caesar;

public class CaesarEncryption implements Cryptor {
	
	@Override
	  public void Encrypt(String eingabe) {
	    String text = "";
	    System.out.println("Eingegeben: " + eingabe);
	    for (int i = 0;i < eingabe.length(); i++){
	        int value = eingabe.charAt(i);
	        String letter = String.valueOf( (char) (value + 2));
	        text += letter;
	    }
	    System.out.println("Encrypted: " + text);
	  }

	  @Override
	  public void Decrypt(String eingabe) {
	    String text = "";
	    System.out.println("Eingegeben: " + eingabe);
	    for (int i = 0;i < eingabe.length(); i++){
	        int value = eingabe.charAt(i);
	        String letter = String.valueOf( (char) (value - 2));
	        text += letter;
	        
	    }
	    System.out.println("Decrypted: " + text);
	  }
}



