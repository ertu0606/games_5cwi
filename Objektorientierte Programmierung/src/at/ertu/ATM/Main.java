package at.ertu.ATM;

public class Main {

	public static void main(String[] args) {
		Controller controller = new Controller();
		GUI gui = new Console();
		float newBalance = 0;
		while(true) {
			int num = gui.question();
			
			switch (num) {
				case 1:
					newBalance = gui.payin();
					controller.addBalance(newBalance);
					break;
				case 2:
					newBalance = gui.payout();
					controller.removeBalance(newBalance);
					break;
				case 3:
					newBalance = controller.getBalance();
					gui.credit(newBalance);
					break;
				case 4:
					System.exit(0);
			}
		}	
	}

}
