package at.ertu.ATM;

public interface GUI {
	public int question();	
	public float payin(); 
	public float payout();
	public void credit(float balance);
}
