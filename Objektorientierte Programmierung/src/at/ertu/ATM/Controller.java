package at.ertu.ATM;

public class Controller {
	private float balance;

	public Controller() {
		super();
		this.balance = 0;
	}

	public float getBalance() {
		return balance;
	}

	public void addBalance(float balance) {
		this.balance += balance;
	} 
	
	public void removeBalance(float balance) {
		this.balance -= balance;
	} 
	
	
}
