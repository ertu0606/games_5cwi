package at.ertu.Interface;

public class Main {
	public static void main(String[] args) {
		Title t1 = new Title();
		Song s1 = new Song();
		Item i1 = new Item();
		Player p1 = new Player();
		
		p1.addPlayable(t1);
		p1.addPlayable(s1);
		p1.addPlayable(i1);
		p1.playAll();
	}
}
