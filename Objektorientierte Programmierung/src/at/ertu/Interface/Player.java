package at.ertu.Interface;

import java.util.ArrayList;
import java.util.List;

public class Player {
	private List<Playable> playables;
	
	public Player() {
		this.playables = new ArrayList<>();
	}
	
	public void addPlayable(Playable p) {
		this.playables.add(p);
	}
	
	public void playAll() {
		for (Playable playable : playables) {
			playable.play();
		}
	}
}
