package at.ertu.Tiktaktoe;

public class Coordinate {
	
	int col;
	int row;
	
	public Coordinate(int col, int row) {
		super();
		this.col = col;
		this.row = row;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}
	
	
	
	
}
