package at.ertu.Tiktaktoe;

import java.lang.reflect.Array;
import java.util.Scanner;

public class Tiktaktoe {
	String[][] field = new String[3][3];

	  boolean player1 = true;
	  Scanner scan = new Scanner(System.in);

	  public void printField() {
	    // TODO Auto-generated method stub
	    System.out.println(field[0][0] + "|" + field[0][1] + "|" + field[0][2]);
	    System.out.println(field[1][0] + "|" + field[1][1] + "|" + field[1][2]);
	    System.out.println(field[2][0] + "|" + field[2][1] + "|" + field[2][2]);
	  }

	  public void initField() {
	    // TODO Auto-generated method stub
	    for (int col = 0; col < 3; col++) {
	      for (int row = 0; row < 3; row++) {
	        field[row][col] = " ";
	      }
	    }
	  }

	  public void setSymbol(int x, int y, boolean player1) {

	    if (player1 == true) {
	      field[x][y] = "x";
	    } else {
	      field[x][y] = "0";
	    }

	  }

	  public void userInput() {
	    if (player1 == true) {
	      System.out.println("Enter your coordinations Player 1 ");
	      String input = scan.next();
	      String[] inputAr = input.split(",");
	      int x = Integer.parseInt(inputAr[0]);
	      int y = Integer.parseInt(inputAr[1]);
	      if (field[x][y] == " ") {
	        setSymbol(x, y, player1);
	      } else {
	        System.out.println("This place isn't empty");
	        userInput();
	      }
	    } else {
	      System.out.println("Enter your coordinations Player 2 ");
	      String input = scan.next();
	      String[] inputAr = input.split(",");
	      int x = Integer.parseInt(inputAr[0]);
	      int y = Integer.parseInt(inputAr[1]);
	      if (field[x][y] == " ") {
	        setSymbol(x, y, player1);
	      } else {
	        System.out.println("This place isn't empty");
	        userInput();
	      }
	    }

	  }
	  
	  public boolean checkWinner() {
		  Boolean winner = false;
		  for(int i=0; i<Array.getLength(field);i++) { 
			  if(field[i][0]==field[i][1]&&field[i][1]==field[i][2]&&field[i][0]!=" ") {
				  winner = true;
				  break;
			  }			  
		  }
		  for(int i=0; i<Array.getLength(field[0]);i++) { 
			  if(field[0][i]==field[1][i]&&field[1][i]==field[2][i]&&field[0][i]!=" ") {
				  winner = true;
				  break;
			  }			  
		  }
		  if(field[0][0]==field[1][1]&&field[1][1]==field[2][2]&&field[0][0]!=" ") {
			  winner = true;
		  }	
		  if(field[0][2]==field[1][1]&&field[1][1]==field[2][0]&&field[0][2]!=" ") {
			  winner = true;
		  }	
		  return winner;
	  }
	  
	 

	  public void game() {
	    initField();
	    printField();
	    while (true) {
	      userInput();
	      printField();
	      boolean winner = checkWinner();
	      if(winner) {
	    	  if(player1) {
	    		  System.out.println("Der Gewinner ist Player1");
	    		  System.exit(0);
	    	  }
	    	  else {
	    		  System.out.println("Der Gewinner ist Player2");
	    		  System.exit(0);
	    	  }	 
	      }
	      player1 = !player1;
	    }
	  }

}

