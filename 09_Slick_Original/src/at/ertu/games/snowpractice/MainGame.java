package at.ertu.games.snowpractice;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;

public class MainGame extends BasicGame {
	public List<CircleActor> snowflakes;
	

	public MainGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		// gezeichnet
		for (CircleActor snowflake : this.snowflakes) {
			snowflake.render(graphics);
		}
		
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		// 1 mal aufgerufen
		this.snowflakes = new ArrayList<CircleActor>();
		for(int i=0; i<40; i++) {
			snowflakes.add(new CircleActor(0));
		}
		for(int i=0; i<40; i++) {
			snowflakes.add(new CircleActor(1));
		}
		for(int i=0; i<40; i++) {
			snowflakes.add(new CircleActor(2));
		}
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// delta = zeit seit dem letzten aufruf
		for (CircleActor snowflake : this.snowflakes) {
			snowflake.update(gc, delta);
		}
	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Practicegame"));
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
	
}
