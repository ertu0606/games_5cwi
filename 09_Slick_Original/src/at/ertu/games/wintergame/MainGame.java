package at.ertu.games.wintergame;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;

import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;

public class MainGame extends BasicGame{
	public List<Snowball> snowballs;

	public MainGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		// TODO Auto-generated method stub
		// gezeichnet
		for (Snowball snowball : this.snowballs) {
			snowball.render(graphics);
		}
		
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub
		// 1 mal aufgerufen
		this.snowballs = new ArrayList<Snowball>();
		for (int i=0; i<40; i++) {
			snowballs.add(new Snowball(0));
		}
		for (int i=0; i<40; i++) {
			snowballs.add(new Snowball(1));
		}
		for (int i=0; i<40; i++) {
			snowballs.add(new Snowball(2));
		}
		
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// TODO Auto-generated method stub
		// delta = zeit seit dem letzten aufruf
		for (Snowball snowball : this.snowballs) {
			snowball.update(gc, delta);
		}
	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
