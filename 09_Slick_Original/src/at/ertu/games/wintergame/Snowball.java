package at.ertu.games.wintergame;

import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Snowball {
	private float x, y;
	private int size;
	private float speed;
	
	public Snowball(int size) {
		super();
		if(size==0) {
			this.size=10;
			this.speed = 0.2f;
		}
		else if(size==1) {
			this.size=20;
			this.speed = 0.4f;
		}
		else{
			this.size = 30;
			this.speed = 0.5f;
		}
		
		setRandomPosition();
	}
	
	private void setRandomPosition() {
		Random random = new Random();
		this.x = random.nextInt(800);
		this.y = random.nextInt(600)-600;
	}
	
	public void update(GameContainer gc, int delta) {
		this.y += delta * this.speed;
		if (this.y>600) {
			setRandomPosition();
		}
	}
	
	public void render(Graphics graphics) {
		graphics.fillOval(this.x, this.y, this.size, this.size);	
	}
	
}
