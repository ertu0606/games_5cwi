package at.ertu.car;

public class Boat extends Vehicle {
	private String propeller;
	private double discount;

	public Boat(String name, String color,double price, String propeller,double discount) {
		super(name,color,price);
		this.propeller = propeller;
		this.discount = discount;
	}

	public String getPropeller() {
		return propeller;
	}

	public void setPropeller(String propeller) {
		this.propeller = propeller;
	}
	
	@Override
	public double getPrice() {
		double fee;
		fee = this.price * discount;
		return fee;
	}
}
