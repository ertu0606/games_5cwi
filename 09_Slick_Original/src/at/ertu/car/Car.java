package at.ertu.car;

public class Car extends Vehicle {
	private int amountoftyres;
	private double discount;

	public Car(String name,String color,double price, int amountoftyres, double discount) {
		super(name,color,price);
		this.amountoftyres = amountoftyres;
		this.discount = discount;
	}

	public int getAmountoftyres() {
		return amountoftyres;
	}

	public void setAmountoftyres(int amountoftyres) {
		this.amountoftyres = amountoftyres;
	}
	@Override
	public double getPrice() {
		double fee;
		fee = this.price * discount;
		return fee;
	}
}
