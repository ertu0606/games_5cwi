package at.ertu.car;

public class Vehicle {
	private String name;
	private String color;
	protected double price;
	private int gas;
	
	public Vehicle(String name, String color, double price) {
		super();
		this.name = name;
		this.color = color;
		this.price = price;
		this.gas = 0;
	}
	
	public void doBlink() {
		System.out.println("blinken");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	public double getPrice() {
	return this.price;
	}
}
