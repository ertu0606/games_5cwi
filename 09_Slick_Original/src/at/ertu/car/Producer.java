package at.ertu.car;

import java.util.ArrayList;
import java.util.List;

public class Producer{
	private String name;
	private List<Vehicle> vehicles;

	public Producer(String name) {
		super();
		this.name = name;
		this.vehicles = new ArrayList<Vehicle>();
	}

	public void printVehicles() {
		for (Vehicle vehicle :vehicles) {
			System.out.println(vehicle.getName());
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValueOfVehicles() {
		int value = 0;
		for(Vehicle vehicle: vehicles) {
			value+= vehicle.getPrice();
		}
		return value;
	}
	
	
	
}
