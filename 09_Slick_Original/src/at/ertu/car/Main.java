package at.ertu.car;

public class Main {	
	public static void main(String[]args) {
	Car c1 = new Car("bmw","blue",2000,4,0.5);
	
	Boat boat1 = new Boat("f1","red",2200,"propeller1",0.9);
	
	Producer p1 = new Producer("bmw group");
	
	Vehicle v1 = new Car("Opel", "brown",5000,4,0.6);
	
	System.out.println(c1.getColor());
	System.out.println(c1.getName());
	
	System.out.println(p1.getName());
	
	
	System.out.println(c1.getPrice());
	System.out.println(boat1.getPrice());

	System.out.println(v1.getColor());
	
	}
}
